/*
 * at_parser.cpp
 *
 *  Created on: Sep 19, 2020
 *      Author: esse
 */

<<<<<<< HEAD
#include "at_parser.h"
#include "main.h"
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "common_list.h"
#include "uart_driver.h"

command_t g_cmd;
urc_t g_urc;
=======

#include "at_parser.h"
#include "main.h"
#include <stdint.h>
#include <stdlib.h>
#include "string.h"
#include "common_list.h"

command_t g_cmd;
urc_t 	g_urc;
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766

uint32_t send_at_cmd(const char at_cmd[], const char *p_expected_response, uint32_t timeout)
{

	void initcmd(command_t cmd);
	uint32_t read_bytes = 0;
	uint8_t *cmd;
<<<<<<< HEAD
	cmd = (uint8_t*) at_cmd;
	uint8_t *rx_buf = (uint8_t*) malloc(RX_BUF_SIZE);
	if (strlen(at_cmd) > 0)
	{
		uart_write(uart_inst, cmd, strlen(at_cmd), false, 100);
		HAL_Delay(10000); //TODO remove and create response list.
		read_bytes = uart_read(uart_inst, rx_buf, RX_BUF_SIZE, 100);
	}
	if (read_bytes > 0)
	{
		g_cmd.cmd = at_cmd;
		if (check_response_status((char *) rx_buf, (char *) p_expected_response))
		{
			g_cmd.length = read_bytes;
			g_cmd.response = split((char *) rx_buf, RECV_AT_SEPARATOR);
=======
	cmd = (uint8_t*)at_cmd;
	uint8_t *rx_buf=(uint8_t*)malloc(1000); //TODO use free() when needed
	if(strlen(at_cmd) > 0)
	{
		volatile HAL_StatusTypeDef ret = HAL_UART_Transmit(&huart2,cmd,strlen(at_cmd),100);
		HAL_Delay(5000); //TODO remove and create response list.
		read_bytes = HAL_UART_Receive(&huart2,rx_buf,100,timeout);
	}
	if(read_bytes >  0)
	{
		g_cmd.cmd = at_cmd;
		if(check_response_status((char *)rx_buf, (char *)p_expected_response))
		{
			g_cmd.length = read_bytes;
			g_cmd.response = split((char *)rx_buf, RECV_AT_SEPARATOR);
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766

		}
	}
	asm("nop");
<<<<<<< HEAD
	free(rx_buf);
	return read_bytes;
}

uint32_t at_send_read_cmd(const char *at_cmd, LIST_T cmd_list, uint32_t timeout)
{
	uint32_t cmd_len = 0;
	char cmd[CMD_LEN];
	cmd_len = snprintf(cmd,CMD_LEN,"%s?%s", at_cmd, AT_SUFIX);
	if(cmd_len > 0)
	{
		uart_write(uart_inst, (uint8_t *)cmd, cmd_len, false, timeout);
		cmd_list = add_after(cmd_list, (char *)at_cmd);
	}

	return cmd_len;
}

uint32_t at_send_test_cmd(const char at_cmd[], LIST_T cmd_list, uint32_t timeout)
{
	uint32_t cmd_len = 0;
	char cmd[CMD_LEN];
	cmd_len = snprintf(cmd, CMD_LEN, "%s=?%s", at_cmd, AT_SUFIX);
	if (cmd_len > 0)
	{
		uart_write(uart_inst, (uint8_t *) cmd, cmd_len, false, timeout);
		cmd_list = add_after(cmd_list, (char *) at_cmd);
	}

	return cmd_len;
}

uint32_t at_send_cmd(const char at_cmd[], LIST_T cmd_list, uint32_t timeout)
{
	uint32_t cmd_len = 0;
	char cmd[CMD_LEN];
	cmd_len = snprintf(cmd, CMD_LEN, "%s%s", at_cmd, AT_SUFIX);
	if (cmd_len > 0)
	{
		uart_write(uart_inst, (uint8_t *) cmd, cmd_len, false, timeout);
		cmd_list = add_after(cmd_list, (char *) at_cmd);
	}

	return cmd_len;

}

//TODO IMPLEMENT send command for different types of AT

uint32_t message_read(uint8_t *read_msg)
{
	uint32_t read_bytes = 0;
	uint8_t *rx_buf = (uint8_t*) malloc(10000);
	read_bytes = uart_read(uart_inst, rx_buf, RX_BUF_SIZE, 100);
	read_msg = rx_buf;
	if (read_bytes > 0)
	{
		LIST_T mlist = new_list();
		if (!check_string_header((char *) rx_buf, (char *) AT, 3))
		{
			mlist = data_to_list(mlist, (char *) rx_buf, (char *) "\r\n\r\n");
			if (!is_null(mlist))
			{
=======
	return read_bytes;
}
//TODO IMPLEMENT send command for different types of AT


uint32_t message_read(void)
{
	uint32_t read_bytes = 0;
	uint8_t *rx_buf=(uint8_t*)malloc(1000);//TODO use free() when needed
	read_bytes = HAL_UART_Receive(&huart2,rx_buf,1000,30000);
	if(read_bytes > 0)
	{
		LIST_T mlist = new_list();
		if(!check_string_header((char *)rx_buf, (char *)AT, 3))
		{
			mlist = data_to_list(mlist, (char *)rx_buf, (char *)"\r\n\r\n");
			if(!is_null(mlist)){
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
				g_urc.urc_list = mlist;
				g_urc.count = count_elements(mlist);
				asm("nop");
			}

		}
	}
<<<<<<< HEAD
	free(rx_buf);
=======
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
	return g_urc.count;
}

uint8_t check_response(const char *response, const char * expected)
{
	uint8_t found = 0;
<<<<<<< HEAD
	if (strstr(response, expected) != NULL)
=======
	if(strstr(response, expected) != NULL )
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
	{
		found = 1;
	}
	return found;
}

<<<<<<< HEAD
const char *get_response(command_t cmd)
{
	if (cmd.length <= 0)
	{
		return (const char*) cmd.length;
	} else
=======


const char *get_response(command_t cmd)
{
	if(cmd.length <= 0)
	{
		return (const char*)cmd.length;
	}else
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
	{
		return cmd.cmd;
	}
}
uint8_t check_string_header(char *rx_arr, char *expected_header, uint32_t length)
{
	char *temp, *dest;
<<<<<<< HEAD
	temp = (char *) malloc(1000);
	dest = (char *) malloc(1000);
	temp = strncpy(dest, rx_arr, length);
	uint8_t state = 0;
	if (strstr(temp, expected_header) != NULL)
	{
		state = 1;
	}
	free(temp);
	free(dest);
=======
	temp = (char *)malloc(1000);//TODO use free() when needed
	dest = (char *)malloc(1000);//TODO use free() when needed
	temp = strncpy(dest, rx_arr, length);
	uint8_t state=0;
	if(strstr(temp, expected_header) != NULL)
	{
		state = 1;
	}
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
	return state;
}

void initcmd(command_t cmd)
{
	cmd.cmd = 0;
	cmd.length = 0;
	cmd.response = 0;
}

<<<<<<< HEAD
char *split(const char *rx_arr, const char *delim)
{
	char *p_result = strstr(rx_arr, delim);

	if (p_result == NULL)
	{
		return NULL;
	}

	*p_result = '\0';
	return p_result + strlen(delim); //TODO NOT RETURN POINTER
}

uint8_t check_response_status(char *rx_arr, char *expected_status)
{
	uint8_t status = 1;
	if (strstr(rx_arr, expected_status) != NULL) //TODO use memmem instead of strstr
=======

char *split(const char *rx_arr, const char *delim)
{
    char *p_result = strstr(rx_arr, delim);

    if (p_result == NULL)
    {
    	return NULL;
    }

    *p_result= '\0';
    return p_result + strlen(delim); //TODO NOT RETURN POINTER
}


uint8_t check_response_status(char *rx_arr, char *expected_status)
{
	uint8_t status = 1;
	if( strstr(rx_arr, expected_status) != NULL) //TODO use memmem instead of strstr
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
	{
		status = 1;
	}
	return status;
}

LIST_T data_to_list(LIST_T list, char *buf, char *delimiter)
{

	char* ptr = strtok(buf, delimiter);
<<<<<<< HEAD
	while (ptr != NULL)
=======
	while(ptr != NULL)
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
	{
		list = add_after(list, ptr);
		ptr = strtok(NULL, delimiter);
	}
	return list;
}
