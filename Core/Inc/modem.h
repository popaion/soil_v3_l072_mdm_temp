/*
 * modem.h
 *
 *  Created on: Oct 24, 2020
 *      Author: esse
 */

#ifndef modem_h_
#define modem_h_
#include <stdint.h>

#ifdef __cplusplus
<<<<<<< HEAD
extern "C"
{
#endif

	class Modem
	{
	public:
		Modem();
		virtual ~Modem();
		uint32_t set_context_state(uint8_t state, uint8_t context_id);
		uint32_t configure_context(uint8_t context_id, const char* protocol_type, const char *apn);
		uint32_t start_socket(void);
		uint32_t close_socket(void);
		uint8_t socket_params_cfg(uint8_t num_retry, uint16_t delay_time, uint8_t err_mode, uint8_t header_type, uint8_t async_mode,
				uint32_t timeout);
		uint8_t connect_multi_socket_mode(uint8_t link_id, const char* type, const char* server, uint32_t port);
		void get_ip(uint8_t* ip_buf);
		uint8_t send_tcp_data(uint8_t link_id, uint32_t msg_len, const char* tcp_msg, uint8_t *response_buffer);
	private:
	};
=======
extern "C" {
#endif


class Modem {
public:
	Modem();
	virtual ~Modem();
	uint32_t set_context_state(uint8_t state, uint8_t context_id);
	uint32_t configure_context(uint8_t context_id, const char* protocol_type, const char *apn );
	uint32_t start_socket(void);
	uint32_t close_socket(void);
	uint8_t socket_params_cfg(uint8_t num_retry, uint16_t delay_time,uint8_t err_mode,
	uint8_t header_type, uint8_t async_mode, uint32_t timeout);
	uint8_t connect_multi_socket_mode(uint8_t link_id, char* type, char* server, uint32_t port);
	void get_ip(uint8_t* ip_buf);

private:
};
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766

#ifdef __cplusplus
}
#endif
<<<<<<< HEAD
=======

>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
#endif /* modem_h_ */
