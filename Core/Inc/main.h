/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
<<<<<<< HEAD
#include "stm32l0xx_hal.h"
#include "stm32l0xx_ll_usart.h"
#include "stm32l0xx_ll_rcc.h"
#include "stm32l0xx_ll_bus.h"
#include "stm32l0xx_ll_cortex.h"
#include "stm32l0xx_ll_system.h"
#include "stm32l0xx_ll_utils.h"
#include "stm32l0xx_ll_pwr.h"
#include "stm32l0xx_ll_gpio.h"
#include "stm32l0xx_ll_dma.h"

#include "stm32l0xx_ll_exti.h"
=======
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define POW_RG_Pin GPIO_PIN_13
#define POW_RG_GPIO_Port GPIOC
#define RFM_DIO1_Pin GPIO_PIN_0
#define RFM_DIO1_GPIO_Port GPIOH
#define RFM_DIO2_Pin GPIO_PIN_1
#define RFM_DIO2_GPIO_Port GPIOH
#define USART2_RTS_Pin GPIO_PIN_1
#define USART2_RTS_GPIO_Port GPIOA
#define AI_LEAF_Pin GPIO_PIN_4
#define AI_LEAF_GPIO_Port GPIOA
#define SCLK_Pin GPIO_PIN_5
#define SCLK_GPIO_Port GPIOA
#define MISO_Pin GPIO_PIN_6
#define MISO_GPIO_Port GPIOA
#define MOSI_Pin GPIO_PIN_7
#define MOSI_GPIO_Port GPIOA
#define WATERMRK_1_Pin GPIO_PIN_0
#define WATERMRK_1_GPIO_Port GPIOB
#define WATERMRK_1_EXTI_IRQn EXTI0_1_IRQn
#define WATERMRK_2_Pin GPIO_PIN_1
#define WATERMRK_2_GPIO_Port GPIOB
#define WATERMRK_2_EXTI_IRQn EXTI0_1_IRQn
#define WATERMRK_3_Pin GPIO_PIN_2
#define WATERMRK_3_GPIO_Port GPIOB
#define WATERMRK_3_EXTI_IRQn EXTI2_3_IRQn
#define SDI_D_Pin GPIO_PIN_12
#define SDI_D_GPIO_Port GPIOB
#define SDI_D_EXTI_IRQn EXTI4_15_IRQn
#define A7670_PWRKEY_Pin GPIO_PIN_13
#define A7670_PWRKEY_GPIO_Port GPIOB
#define POW_LEAF_Pin GPIO_PIN_14
#define POW_LEAF_GPIO_Port GPIOB
#define POW_1WR_Pin GPIO_PIN_15
#define POW_1WR_GPIO_Port GPIOB
#define RG_SENSOR_Pin GPIO_PIN_8
#define RG_SENSOR_GPIO_Port GPIOA
#define RG_SENSOR_EXTI_IRQn EXTI4_15_IRQn
#define PYC_TX_Pin GPIO_PIN_9
#define PYC_TX_GPIO_Port GPIOA
#define PYC_RX_Pin GPIO_PIN_10
#define PYC_RX_GPIO_Port GPIOA
#define INT_LEAF_Pin GPIO_PIN_11
#define INT_LEAF_GPIO_Port GPIOA
#define DIS_LEAF_Pin GPIO_PIN_12
#define DIS_LEAF_GPIO_Port GPIOA
#define POW_12V_Pin GPIO_PIN_15
#define POW_12V_GPIO_Port GPIOA
#define RFM_DIO0_Pin GPIO_PIN_3
#define RFM_DIO0_GPIO_Port GPIOB
#define RFM_CS_Pin GPIO_PIN_4
#define RFM_CS_GPIO_Port GPIOB
#define RFM_DIO3_Pin GPIO_PIN_5
#define RFM_DIO3_GPIO_Port GPIOB
#define A7670_RESET_Pin GPIO_PIN_8
#define A7670_RESET_GPIO_Port GPIOB
#define RG_SENSOR1_Pin GPIO_PIN_9
#define RG_SENSOR1_GPIO_Port GPIOB
#define RG_SENSOR1_EXTI_IRQn EXTI4_15_IRQn
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

<<<<<<< HEAD
=======
extern UART_HandleTypeDef huart2;

>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
