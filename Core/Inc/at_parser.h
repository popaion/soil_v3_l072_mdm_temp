/*
 * at_parser.h
 *
 *  Created on: Sep 19, 2020
 *      Author: esse
 */

#ifndef INC_AT_PARSER_H_
#define INC_AT_PARSER_H_

<<<<<<< HEAD
#include <stdint.h>
#include "string.h"
#include "common_list.h"
#include "uart_driver.h"
=======

#include <stdint.h>
#include "string.h"
#include "common_list.h"
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766

#ifdef __cplusplus
extern "C"
{
#endif /*#ifdef __cplusplus*/

#define AT						"AT"
#define RECV_AT_SEPARATOR 		"\r\r\n"
#define AT_SUFIX  				"\r\n"
#define AT_OK					"\n\rOK\r\n"
#define AT_ERROR				"\n\rERROR\r\n"
#define ACT						1
#define DEACT					0
#define CONTEXT_ID				1
#define AT_CONTEXT_ACTIVATE		"AT+CGACT"
#define AT_CONTEXT_CFG			"AT+CGDCONT"
#define AT_START_SOCKET			"AT+NETOPEN"
#define AT_STOP_SOCKET			"AT+NETCLOSE"
#define AT_SOCKET_CFG			"AT+CIPCCFG"
#define AT_IP					"IP"
#define AT_IPADDR				"AT+IPADDR"
<<<<<<< HEAD
#define AT_CGATT				"AT+CGATT"
#define AT_UDP					"UDP"
#define AT_TCP					"TCP"
#define AT_CONNECTION_OPEN		"AT+CIPOPEN"
#define AT_TCP_SEND 			"AT+CIPSEND"
#define RX_BUF_SIZE				5000
#define CMD_LEN 				100

	typedef struct Command
	{
		const char *cmd;
		uint32_t length;
		char *response;
	} command_t;

	typedef struct Urc
	{
		LIST_T urc_list;
		uint32_t count;
	} urc_t;

	extern command_t g_cmd;
	extern urc_t g_urc;
	extern uart_driver_instance_t *uart_inst;
	extern LIST_T command_list;

//send at command and return number of received bytes
	uint32_t send_at_cmd(const char at_cmd[], const char *p_expected_response, uint32_t timeout);
	uint32_t at_send_read_cmd(const char *at_cmd, LIST_T cmd_list, uint32_t timeout);
	uint32_t at_send_test_cmd(const char at_cmd[], LIST_T cmd_list, uint32_t timeout);
	uint32_t at_send_cmd(const char at_cmd[], LIST_T cmd_list, uint32_t timeout);
//read URC and return number of URCs received
	uint32_t message_read(uint8_t *read_msg);
	uint8_t check_response(const char *response, const char * expected);
	uint8_t set_response(const char *cmd, uint32_t length, uint8_t is_urc);
	const char *get_response(command_t cmd);
	uint8_t *process_response(command_t cmd);
	void initcmd(command_t cmd);
	char *split(const char *array, const char *delim);
	uint8_t check_string_header(char *array, char *expected_header, uint32_t length);
	uint8_t check_response_status(char *array, char *expected_status);
	LIST_T data_to_list(LIST_T list, char *buf, char *delimiter);
=======
#define CMD_LEN 				100
#define AT_UDP					"UDP"
#define AT_TCP					"TCP"
#define AT_CONNECTION_OPEN		"AT+CIPOPEN"


typedef struct Command
{
  const char *cmd;
  uint32_t length;
  char *response;
} command_t;

typedef struct Urc
{
	LIST_T urc_list;
	uint32_t count;
} urc_t;

extern command_t 	g_cmd;
extern urc_t 	  	g_urc;


//send at command and return number of received bytes
uint32_t send_at_cmd(const char at_cmd[], const char *p_expected_response, uint32_t timeout);
//read URC and return number of URCs received
uint32_t message_read(void);
uint8_t check_response(const char *response, const char * expected);
uint8_t set_response(const char *cmd, uint32_t length, uint8_t is_urc);
const char *get_response(command_t cmd);
uint8_t *process_response(command_t cmd);
void initcmd(command_t cmd);
char *split(const char *array, const char *delim);
uint8_t check_string_header(char *array, char *expected_header, uint32_t length);
uint8_t check_response_status(char *array, char *expected_status);
LIST_T data_to_list(LIST_T list, char *buf, char *delimiter);
>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766

#ifdef __cplusplus
}
#endif /*#ifdef __cplusplus*/
#endif /* INC_AT_PARSER_H_ */

<<<<<<< HEAD
=======

>>>>>>> 0bbbb560a0bcbf996cea3b5cf0451c2cd234f766
